﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UniversitySystem.Web.Controllers
{
    public class TeachersController : Controller
    {

        #region URL Routing

        // GET: Teachers
        public ActionResult Index()
        {
            return View("Teachers");
        }

        // GET: Teachers/Teachers
        public ActionResult Teachers()
        {
            return View("Teachers");
        }

        #endregion


    }
}