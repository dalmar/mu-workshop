﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversitySystem.BOL;
using Z.EntityFramework.Plus;

namespace UniversitySystem.DAL.Repository
{
    public class ClassDL
    {
        private UniversityDBEntities _db;

        public ClassDL()
        {
            _db = new UniversityDBEntities();
        }

        public IEnumerable<Class> GetAll()
        {
            List<Class> classes = _db.Classes.ToList();
            return classes;
        }

        public Class GetById(int ClassId)
        {
            Class objClass = _db.Classes.Find(ClassId);
            return objClass;
        }

        public Feedback Save(Class entity)
        {
            Feedback feedback = new Feedback();
            try
            {
                bool isSuccess = false;

                _db.Classes.Add(entity);
                isSuccess = _db.SaveChanges() > 0;

                if (isSuccess)
                    feedback = BuildFeedbackMessages.BuildSuccessFeedback("Record successfully saved");
                else
                    feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed, please try again");

            }
            catch (Exception)
            {
                feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed due to exception");
            }
            return feedback;
        }
        
        public Feedback Update(Class entity)
        {
            Feedback feedback = new Feedback();
            try
            {
                Class objClass = this.GetById(entity.ClassId);

                if (objClass == null)
                    throw new Exception("Invalid parameter has been passed");

                int rowsAffected = _db.Classes
                                        .Where(x => x.ClassId == entity.ClassId)
                                        .Update(x => new Class()
                                        {

                                        });
                
                if (rowsAffected > 0)
                    feedback = BuildFeedbackMessages.BuildSuccessFeedback("Record successfully updated");
                else
                    feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed, please try again");

            }
            catch (Exception)
            {
                feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed due to exception");
            }
            return feedback;
        }

        public Feedback Delete(Class entity)
        {
            Feedback feedback = new Feedback();
            try
            {
                Class objClass = this.GetById(entity.ClassId);

                if (objClass == null)
                    throw new Exception("Invalid parameter has been passed");

                bool isSuccess = false;

                _db.Entry(objClass).State = EntityState.Deleted;
                isSuccess = _db.SaveChanges() > 0;

                if (isSuccess)
                    feedback = BuildFeedbackMessages.BuildSuccessFeedback("Record successfully deleted");
                else
                    feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed, please try again");

            }
            catch (Exception)
            {
                feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed due to exception");
            }
            return feedback;
        }

        public Feedback Delete(int ClassId)
        {
            Feedback feedback = new Feedback();
            try
            {
                Class objClass = this.GetById(ClassId);

                if (objClass == null)
                    throw new Exception("Invalid parameter has been passed");

                bool isSuccess = false;

                _db.Entry(objClass).State = EntityState.Deleted;
                isSuccess = _db.SaveChanges() > 0;

                if (isSuccess)
                    feedback = BuildFeedbackMessages.BuildSuccessFeedback("Record successfully deleted");
                else
                    feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed, please try again");

            }
            catch (Exception)
            {
                feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed due to exception");
            }
            return feedback;
        }

    }
}
