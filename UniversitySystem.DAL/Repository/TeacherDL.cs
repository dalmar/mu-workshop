﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversitySystem.BOL;
using Z.EntityFramework.Plus;

namespace UniversitySystem.DAL.Repository
{
    public class TeacherDL
    {
        private UniversityDBEntities _db;

        public TeacherDL()
        {
            _db = new UniversityDBEntities();
        }

        public IEnumerable<Teacher> GetAll()
        {
            List<Teacher> teachers = _db.Teachers.ToList();
            return teachers;
        }

        public Teacher GetById(int TeacherId)
        {
            Teacher objTeacher = _db.Teachers.Find(TeacherId);
            return objTeacher;
        }

        public Feedback Save(Teacher entity)
        {
            Feedback feedback = new Feedback();
            try
            {
                bool isSuccess = false;

                _db.Teachers.Add(entity);
                isSuccess = _db.SaveChanges() > 0;

                if (isSuccess)
                    feedback = BuildFeedbackMessages.BuildSuccessFeedback("Record successfully saved");
                else
                    feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed, please try again");

            }
            catch (Exception)
            {
                feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed due to exception");
            }
            return feedback;
        }

        public Feedback Update(Teacher entity)
        {
            Feedback feedback = new Feedback();
            try
            {
                Teacher objTeacher = this.GetById(entity.TeacherId);

                if (objTeacher == null)
                    throw new Exception("Invalid parameter has been passed");

                int rowsAffected = _db.Teachers
                                        .Where(x => x.TeacherId == entity.TeacherId)
                                        .Update(x => new Teacher()
                                        {

                                        });

                if (rowsAffected > 0)
                    feedback = BuildFeedbackMessages.BuildSuccessFeedback("Record successfully updated");
                else
                    feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed, please try again");

            }
            catch (Exception)
            {
                feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed due to exception");
            }
            return feedback;
        }

        public Feedback Delete(Teacher entity)
        {
            Feedback feedback = new Feedback();
            try
            {
                Teacher objTeacher = this.GetById(entity.TeacherId);

                if (objTeacher == null)
                    throw new Exception("Invalid parameter has been passed");

                bool isSuccess = false;

                _db.Entry(objTeacher).State = EntityState.Deleted;
                isSuccess = _db.SaveChanges() > 0;

                if (isSuccess)
                    feedback = BuildFeedbackMessages.BuildSuccessFeedback("Record successfully deleted");
                else
                    feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed, please try again");

            }
            catch (Exception)
            {
                feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed due to exception");
            }
            return feedback;
        }

        public Feedback Delete(int TeacherId)
        {
            Feedback feedback = new Feedback();
            try
            {
                Teacher objTeacher = this.GetById(TeacherId);

                if (objTeacher == null)
                    throw new Exception("Invalid parameter has been passed");

                bool isSuccess = false;

                _db.Entry(objTeacher).State = EntityState.Deleted;
                isSuccess = _db.SaveChanges() > 0;

                if (isSuccess)
                    feedback = BuildFeedbackMessages.BuildSuccessFeedback("Record successfully deleted");
                else
                    feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed, please try again");

            }
            catch (Exception)
            {
                feedback = BuildFeedbackMessages.BuildErrorFeedback("Operation failed due to exception");
            }
            return feedback;
        }

    }
}
