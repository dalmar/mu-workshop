﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversitySystem.BOL;
using UniversitySystem.DAL.Repository;

namespace UniversitySystem.BLL
{
    public class ClassBL
    {
        private readonly ClassDL classDL;

        public ClassBL()
        {
            classDL = new ClassDL();
        }

        public IEnumerable<Class> GetAll()
        {
            return classDL.GetAll();
        }
        
        public Class GetById(int classId)
        {
            return classDL.GetById(classId);
        }

        public Feedback Save(Class entity)
        {            
            List<string> errorsList = new List<string>();

            if (string.IsNullOrEmpty( entity.ClassName) || string.IsNullOrWhiteSpace(entity.ClassName))
                errorsList.Add("Class name field is required");

            if (errorsList.Count > 0)
                return BuildFeedbackMessages.BuildErrorFeedback(errorsList);

            return classDL.Save(entity);
        }


        public Feedback Update(Class entity)
        {
            List<string> errorsList = new List<string>();

            if (entity.ClassId <= 0)
                errorsList.Add("Class Id is required");

            if (string.IsNullOrEmpty(entity.ClassName) || string.IsNullOrWhiteSpace(entity.ClassName))
                errorsList.Add("Class name field is required");

            if (errorsList.Count > 0)
                return BuildFeedbackMessages.BuildErrorFeedback(errorsList);

            return classDL.Update(entity);
        }
        

        public Feedback Delete(Class entity)
        {
            List<string> errorsList = new List<string>();

            if (entity.ClassId <= 0)
                errorsList.Add("Class Id is required");

            if (errorsList.Count > 0)
                return BuildFeedbackMessages.BuildErrorFeedback(errorsList);

            return classDL.Delete(entity);
        }


        public Feedback Delete(int ClassId)
        {
            List<string> errorsList = new List<string>();

            if (ClassId <= 0)
                errorsList.Add("Class Id is required");

            if (errorsList.Count > 0)
                return BuildFeedbackMessages.BuildErrorFeedback(errorsList);

            return classDL.Delete(ClassId);
        }

    }
}
