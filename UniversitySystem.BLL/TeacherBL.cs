﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversitySystem.BOL;
using UniversitySystem.DAL.Repository;

namespace UniversitySystem.BLL
{
    public class TeacherBL
    {
        private readonly TeacherDL teacherDL;

        public TeacherBL()
        {
            teacherDL = new TeacherDL();
        }

        public IEnumerable<Teacher> GetAll()
        {
            return teacherDL.GetAll();
        }

        public Teacher GetById(int classId)
        {
            return teacherDL.GetById(classId);
        }

        public Feedback Save(Teacher entity)
        {
            List<string> errorsList = new List<string>();

            if (string.IsNullOrEmpty(entity.TeacherFirstName) || string.IsNullOrWhiteSpace(entity.TeacherFirstName))
                errorsList.Add("First name field is required");

            if (errorsList.Count > 0)
                return BuildFeedbackMessages.BuildErrorFeedback(errorsList);

            return teacherDL.Save(entity);
        }


        public Feedback Update(Teacher entity)
        {
            List<string> errorsList = new List<string>();

            if (entity.TeacherId <= 0)
                errorsList.Add("Teacher Id is required");

            if (string.IsNullOrEmpty(entity.TeacherFirstName) || string.IsNullOrWhiteSpace(entity.TeacherFirstName))
                errorsList.Add("Teacher name field is required");

            if (errorsList.Count > 0)
                return BuildFeedbackMessages.BuildErrorFeedback(errorsList);

            return teacherDL.Update(entity);
        }


        public Feedback Delete(Teacher entity)
        {
            List<string> errorsList = new List<string>();

            if (entity.TeacherId <= 0)
                errorsList.Add("Teacher Id is required");

            if (errorsList.Count > 0)
                return BuildFeedbackMessages.BuildErrorFeedback(errorsList);

            return teacherDL.Delete(entity);
        }


        public Feedback Delete(int TeacherId)
        {
            List<string> errorsList = new List<string>();

            if (TeacherId <= 0)
                errorsList.Add("Teacher Id is required");

            if (errorsList.Count > 0)
                return BuildFeedbackMessages.BuildErrorFeedback(errorsList);

            return teacherDL.Delete(TeacherId);
        }

    }
}
