﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversitySystem.BOL
{
    public class Feedback
    {
        public bool IsValid { get; set; }
        public List<string> Errors { get; set; }
        public string Success { get; set; }
    }
}
