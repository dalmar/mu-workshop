﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversitySystem.BOL
{
    public static class BuildFeedbackMessages
    {
        public static Feedback BuildErrorFeedback(string error)
        {
            Feedback feedback = new Feedback();
            feedback.IsValid = false;
            feedback.Errors.Add(error);
            return feedback;
        }

        public static Feedback BuildErrorFeedback(List<string> errors)
        {
            Feedback feedback = new Feedback();
            feedback.IsValid = false;
            feedback.Errors.AddRange(errors);
            return feedback;
        }

        public static Feedback BuildSuccessFeedback(string successMsg)
        {
            Feedback feedback = new Feedback();
            feedback.IsValid = true;
            feedback.Success = successMsg;
            return feedback;
        }

    }
}
